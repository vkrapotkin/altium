program TextGenerator;

uses
  Vcl.Forms,
  UGeneratorMainForm in 'UGeneratorMainForm.pas' {fGeneratorMain},
  UTextGenerator in 'UTextGenerator.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfGeneratorMain, fGeneratorMain);
  Application.Run;
end.
