unit UGeneratorMainForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls,
  System.Generics.Collections,
  UTextGenerator;

type
  TState = (sStopped, sRunning);
  TfGeneratorMain = class(TForm)
    eLineCount: TLabeledEdit;
    eWordCountMin: TLabeledEdit;
    eWordCountMax: TLabeledEdit;
    SaveDialog1: TSaveDialog;
    pb1: TProgressBar;
    bStartStop: TButton;
    lblDict: TLabel;
    m1: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure bStartStopClick(Sender: TObject);
    procedure eLineCountKeyPress(Sender: TObject; var Key: Char);
  private
    FState: TState;
    procedure PrepareDictionary;
    procedure GenerateFile();
    procedure Finish(Sender: Tobject);
    procedure SetState(const Value: TState);
  public
    FFile:TBufferedFileStream;
    FWords: TStringList;
    FThread: TGeneratorThread;
    property State:TState read FState write SetState;
  end;

var
  fGeneratorMain: TfGeneratorMain;

implementation

uses
  system.ioutils;


{$R *.dfm}

procedure TfGeneratorMain.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FWords);
end;

// makes a base dictionary from some source
procedure TfGeneratorMain.PrepareDictionary;
var
  f: TStreamReader;
  fname: string;
  s: string;
  arr: TArray<string>;
  i: Integer;
begin
  if (ParamCount>0) and FileExists(ParamStr(1)) then
    fname := ParamStr(1)
  else
    fname := ExtractFilePath(ParamStr(0)) + 'onegin.txt';
  f := TStreamReader.Create(fname, TEncoding.UTF8, false, 40000);
  while not f.EndOfStream do
  begin
    s := f.ReadLine.Trim;
    if s.Length<3 then
      Continue;

    arr := s.Split([' ','.',',','!',';','-','=','(',')',':','�','�','�','"','[',']','?','�','�','>','<']);
    for i := 0 to High(arr) do
    begin
      if arr[i].Length<3 then
        Continue;
      if '0123456789.'.Contains(arr[i].Chars[0]) then
        Continue;

      FWords.Add(arr[i]);
    end;

  end;
end;

procedure TfGeneratorMain.SetState(const Value: TState);
begin
  FState := Value;
  if Value=sStopped then
  begin
    bStartStop.Caption := '������';
    if FThread<>nil then
      FThread.Terminate;
  end
  else
  begin
    bStartStop.Caption := '����������';
    GenerateFile();
  end;
end;


// thread OnTerminate handler
procedure TfGeneratorMain.eLineCountKeyPress(Sender: TObject; var Key: Char);
begin
  if (KEY=#13)and (State=sStopped) then
    State := sRunning;
end;

procedure TfGeneratorMain.Finish(Sender:Tobject);
var
  th: TGeneratorThread absolute Sender;
begin
  FThread:=nil;
  State := sStopped;
  ShowMessageFmt('����������� ����: %s'#13#10+
                 '������: %s'#13#10+
                 '�����: %s'#13#10+
                 '��������� �������: %s',[
                  th.FName, FormatFloat(',0', th.FSize), FormatFloat(',0', th.FLineCount),
                  FormatDateTime('hh:nn:ss', now-th.FStartTime)
                 ]);
end;

// run a thread which generates an output file
procedure TfGeneratorMain.GenerateFile();
var
  LineCount, WMax, WMin:integer;
begin
  LineCount := StrToIntDef(eLineCount.Text, 10000)*1000;
  WMin := StrToIntDef(eWordCountMin.Text,2);
  WMax := StrToIntDef(eWordCountMax.Text,10);

  FThread := TGeneratorThread.Create(LineCount div 1, WMin, WMax, FWords, pb1, FFile, Finish);
  FThread.Start;
end;

procedure TfGeneratorMain.bStartStopClick(Sender: TObject);
begin
  if State=sStopped then
    State := sRunning
  else
    State := sStopped;
end;

procedure TfGeneratorMain.FormCreate(Sender: TObject);
begin
  FWords := TStringList.Create;
  FWords.Sorted := true;
  FWords.Duplicates := TDuplicates.dupIgnore;

  PrepareDictionary;

  lblDict.Caption := '���� � �������: ' + FWords.Count.ToString;
  m1.Lines.Assign(FWords);
end;

end.
