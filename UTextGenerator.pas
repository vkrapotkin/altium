unit UTextGenerator;

interface
uses
   System.Classes, System.Threading, System.SyncObjs,
   Vcl.ComCtrls;

type
  TGeneratorThread = class(TThread)
  const
    BUF_SIZE= 100000;
  private
    t:int64;
    function TimeToUpdate(interval: int64): boolean; inline;
  public
    FStartTime : TDateTime;
    FLineCount, FWordsMin, FWordsMax: NativeInt; // limits of line generation
    FWords: TStringList;
    FPBar: TProgressBar;
    FName: string;
    FSize: Int64;
    FFile:TBufferedFileStream;
    procedure Execute; override;
    constructor Create(ALineCount, AWordsMin, AWordsMax: NativeInt;
      AWords: TStringList; APBar:TProgressBar;
      AFile:TFileStream;
      ATermProc:TNotifyEvent);
  end;

implementation
uses
  System.sysutils, system.ioutils;


{ TGeneratorThread }

constructor TGeneratorThread.Create(ALineCount, AWordsMin, AWordsMax: NativeInt;
      AWords: TStringList; APBar:TProgressBar;
      AFile:TFileStream;
      ATermProc:TNotifyEvent);
begin
  inherited Create(true);
  FLineCount := ALineCount;
  FWordsMin := AWordsMin;
  FWordsMax := AWordsMax;
  FWords := AWords;
  FPBar := APBar;
  FPBar.Max := ALineCount;
  OnTerminate := ATermProc;
  FreeOnTerminate := true;
end;

procedure TGeneratorThread.Execute;
var
  i, j: NativeInt;
  sb: TStringBuilder;
  repeats: integer;
  num, body:string;
  arr: TBytes;
begin
  fname := ExtractFilePath(ParamStr(0)) + 'hugeFile.txt';
  if FileExists(fname) then
    tfile.Delete(fname);

  FStartTime := now;

  ffile := TBufferedFileStream.Create(fname,  fmCreate, BUF_SIZE);
  sb := TStringBuilder.Create(2048);
  try
    i := 0;
    while i < FLineCount do
    begin
      if Terminated then
        exit;

      // update UI every 500 msec
      if  TimeToUpdate(500) or (i = FlineCount-1) then
      begin
        Synchronize(procedure begin
          FPBar.Position := i;
        end);
      end;

      // StringBuilder is used to avoid memory manager overhead
      sb.Length:=0;
      for j := 0 to FWordsMin + Random(FWordsMax - FWordsMin + 1) -1 do
      begin
        sb.Append( FWords[Random(FWords.Count)]);
        if j<>FWordsMax then
          sb.Append(' ');
      end;
      body:=sb.ToString;

      // we need to have some repeating values
      // let it be 1 per cent for every of 2, 3, 4 repeats
      case Random(100) of
        97: repeats := 2;
        98: repeats := 3;
        99: repeats := 4;
        else
          repeats := 1;
      end;
      for j := 0 to Repeats-1 do
      begin
        num := (1+Random(FLineCount div 4)).ToString;
        sb.length := 0;
        sb.append(num).append('. ').AppendLine(body);
        arr:=TEncoding.UTF8.GetBytes(sb.ToString);
        ffile.Write(arr, Length(arr));
        Inc(i);
        if i >= FLineCount then
          break;
      end;
    end;
    // store the result file's size;
    FSize := ffile.Size;
  finally
    FFile.FlushBuffer;
    FFile.Free;
    sb.Free;
  end;

end;

function TGeneratorThread.TimeToUpdate(interval: int64): boolean;
begin
  result := tthread.gettickcount-t >= interval;
  if result then
    t := tthread.gettickcount;
end;


end.
