unit USorterMainForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.StdCtrls, System.Generics.Collections,
  USorterThreads, Vcl.ExtCtrls;

type
  TState = (sStopped, sRunning);

  TIndexRec = packed record
    N, S, L: NativeInt;
  end;

  TfSorterMain = class(TForm)
    dlgOpen1: TOpenDialog;
    m1: TMemo;
    pnl1: TPanel;
    pnl2: TPanel;
    pnl3: TPanel;
    lblFilename: TLabel;
    lblFilesize: TLabel;
    lblInfo: TLabel;
    bStartStop: TButton;
    pb1: TProgressBar;
    bOpenFile: TButton;
    bIndex: TButton;
    rbAlf: TRadioButton;
    rbCommon: TRadioButton;
    spl1: TSplitter;
    m2: TMemo;
    procedure bOpenFileClick(Sender: TObject);
    procedure bStartStopClick(Sender: TObject);
    procedure bIndexClick(Sender: TObject);
  private
    FState: TState;
    procedure SetState(const Value: TState);
    procedure StartSorting;
    // sort thread's OnTerminate handler
    procedure SortTerm(Sender: TObject);
    // try to open selected file and show its name and size
    procedure OpenFile(AFilename: string);

    // handlers of thread messages to update UI
    procedure UmSetMax(var Message:TMessage); message UM_SET_MAX;
    procedure UmSetPos(var Message:TMessage); message UM_SET_POS;
    procedure UmLblText(var Message:TMessage); message UM_LBL_TEXT;
    procedure UmLog(var Message:TMessage); message UM_LOG;
    procedure MakeIndexFile;
    procedure WriteResultFile;
    procedure Sort;
  public
    FFilename:string;
    FIndexFName: string;
    FDstFName: string;
    FStrCount: NativeInt;
    FSortThread : TSortThread;
    FIndexFileSize : Int64;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Log(s:string);

    property State:TState read FState write SetState;
  end;

var
  fSorterMain: TfSorterMain;

const
  sz = SizeOf(TIndexRec);

implementation

uses
  System.IOUtils;

{$R *.dfm}


procedure TfSorterMain.MakeIndexFile();
var
  indexFile: TBufferedFileStream;
  srcReader : TStreamReader;
  t: Int64;
  p: TIndexRec;
  s: string;
  i: integer;
begin
  if FileExists(FIndexFName) then
    TFile.Delete(FIndexFName);

  FStrCount := 0;
  indexFile := TBufferedFileStream.Create(FIndexFName, fmCreate, 4096);
  srcReader := TStreamReader.Create(FFilename, TEncoding.UTF8, true);
  try
    p.N := 0;
    repeat
      s := srcReader.ReadLine;
      i := 1;
      while s.Chars[i]<>'.' do
        Inc(i);
      p.S := p.N + i + 2;
      p.L := TEncoding.utf8.GetByteCount(s) + 2;
      indexFile.Write(p, Sz);
      p.N := p.N + p.L;
      Inc(FStrCount);
    until srcReader.EndOfStream;
    FIndexFileSize := indexFile.Size;
  finally
    FreeAndNil(srcReader);
    FreeAndNil(indexFile);
  end;
end;


procedure TfSorterMain.Sort();
var
  sb: TStringBuilder;
  indexFile : TBufferedFileStream;
  srcBinary : TBufferedFileStream;
  numArr: array [0..9] of byte;
  tempArr: array [0..9] of byte;
  Arr: array [0..2999] of byte;
  i: Integer;
  t: int64;

  function GetStr(index:nativeint; original:boolean = false):string;
  var
    r, i: nativeint;
    p: TIndexRec;
    numLen: NativeInt;
    ch:Char;
    w: Cardinal;
    w1, w2, w3, w4: cardinal;
    strLen: integer;
  begin
//    FillChar(arr[0],Length(Arr), 0);
    indexFile.Position := index * sz;
    r := indexFile.Read(p, sz);
    srcBinary.Position := p.N;
    numLen := p.S-p.N-2;
    srcBinary.Read(tempArr, numlen+2);
    strLen := p.L - numLen - 2;
    r := srcBinary.Read(Arr[0], strLen);
    if ((r>2)and(arr[r-2]=13)and(arr[r-1]=10)) then
    begin
      dec(r,2);
      Dec(strlen,2);
    end
    else
    if ((r>1)and(arr[r-1]=13)) then
    begin
      dec(r,1);
      Dec(strlen,1);
    end;
    Move(numarr[0], arr[r], 10-numLen);
    Move(temparr[0], arr[r+10-numLen], numLen);
    Arr[r+10] := 0;
    // UTF8 MBstring parsing
    i := 0;
    sb.Length := 0;
    while i<strlen+10 do
    begin
      if Arr[i]<$80 then
      begin
        ch := char(Arr[i]);
        sb.Append(ch);
        i := i + 1;
      end
      else if (Arr[i] and $E0) = $C0 then
      begin
        w1 := Arr[i] and $1F;
        w2 := Arr[i+1] and $3F;
        ch := char(w1 shl 8 shr 2 or w2);
        sb.Append(ch);
        i := i + 2;
      end
      else if (Arr[i] and $F0) = $E0 then
      begin
        w1 := Arr[i] and $1F;
        w2 := Arr[i+1] and $3F;
        w3 := Arr[i+2] and $3F;
        ch := char(w1 shl 16 shr 4 or w2 shl 8 shr 2 or w3);
        i := i + 3;
      end
      else if (Arr[i] and $F8) = $F0 then
      begin
        w1 := Arr[i] and $1F;
        w2 := Arr[i+1] and $3F;
        w3 := Arr[i+2] and $3F;
        w4 := Arr[i+4] and $3F;
        ch := char(w1 shl 24 shr 6 or w2 shl 16 shr 4 or w3 shl 8 shr 2 or w4);
        i := i + 4;
      end
      else
      begin
        raise Exception.Create('bad encoding. Src offset='+IntToHex(p.n,8)+' len='+p.l.tostring);
      end;
    end;
    Result := sb.ToString;
  end;

  procedure swap(i,j: nativeint);
  var a,b: TIndexRec;
  begin
    indexFile.position := i * sz;
    indexFile.read(a, sz);

    indexFile.position := j * sz;
    indexFile.read(b, sz);

    indexFile.position := j * sz;
    indexFile.Write(a, sz);

    indexFile.position := i * sz;
    indexFile.Write(b, sz);
  end;

  procedure QSort(L, R: Integer; level: integer);
  var
    I, J, P: Integer;
    s1, s2 : string;
  begin
    if tthread.GetTickCount-t > 100 then
    begin
      Log('L='+FormatFloat(',0',L)+' R='+FormatFloat(',0',R)+' Level='+FormatFloat(',0',Level) );
      t := tthread.GetTickCount;
      Application.ProcessMessages;
    end;


    if L < R then
    begin
    repeat
      if (R - L) = 1 then
      begin
        s1 := GetStr(L);
        s2 := GetStr(R);
        if AnsiCompareStr(s1, s2) > 0 then
          swap(L, R);
        break;
      end;
      I := L;
      J := R;
      P := (L + R) shr 1;
      repeat
        while true do
        begin
          s1 := GetStr(i);
          s2 := GetStr(p);
          if AnsiCompareStr(s1, s2) >= 0 then
            break;
          Inc(I);
        end;
        while true do
        begin
          s1 := GetStr(j);
          s2 := GetStr(p);
          if AnsiCompareStr(s1, s2) <= 0 then
            break;
          Dec(J);
        end;
        if I <= J then
        begin
          if I <> J then
            swap(I, J);
          if P = I then
            P := J
          else if P = J then
            P := I;
          Inc(I);
          Dec(J);
        end;
      until I > J;
      if (J - L) > (R - I) then
      begin
        if I < R then
          QSort(I, R, level+1);
        R := J;
      end
      else
      begin
        if L < J then
          QSort(L, J, level+1);
        L := I;
      end;
      until L >= R;
    end;
  end;

begin
  t := tthread.GetTickCount;
  sb:= TStringBuilder.Create(4096);
  indexFile := TBufferedFileStream.Create(FIndexFName, fmOpenReadWrite, 4096);
  srcBinary := TBufferedFileStream.Create(FFilename, fmOpenRead or fmShareDenyWrite, FIndexFileSize);
  try
    FillChar(numArr, 10, Ord('0'));
//    for i := 0 to 999 do
//      swap(Random(FStrCount), Random(FStrCount));
    qsort(0, fstrcount-1, 0);
  finally
    FreeAndNil(sb);
    FreeAndNil(srcBinary);
    FreeAndNil(indexFile);
  end;
end;



procedure TfSorterMain.WriteResultFile();
var
  indexFile : TBufferedFileStream;
  dstBinary : TBufferedFileStream;
  srcBinary : TBufferedFileStream;
  r: integer;
  arr: array[0..2999] of byte;
  p: TIndexRec;
begin
  if FileExists(FDstFName) then
    TFile.Delete(FDstFName);

  indexFile := TBufferedFileStream.Create(FIndexFName, fmOpenRead or fmShareDenyWrite, 4096);
  dstBinary := TBufferedFileStream.Create(FDstFName, fmCreate, 4096);
  srcBinary := TBufferedFileStream.Create(FFilename, fmOpenRead or fmShareDenyWrite, 4096);
  try
    repeat
      indexFile.Read(p, sz);
      srcBinary.Position := p.N;
      r := srcBinary.Read(arr[0], p.L);
      dstBinary.Write(arr[0], p.L);
    until indexFile.position >= indexfile.Size;
  finally
    FreeAndNil(srcBinary);
    FreeAndNil(dstBinary);
    FreeAndNil(indexFile);
  end;
end;


procedure TfSorterMain.bIndexClick(Sender: TObject);
var
  t: int64;

begin
  FIndexFName := ChangeFileExt(FFilename, '.index.bin');
  FDstFName := ChangeFileExt(FFilename, '.out'+ExtractFileExt(FFilename));

  t := TThread.GetTickCount;
  Log('Creating index');
  makeIndexFile();
  Log('Index has been created in '+FormatFloat(',0', TThread.GetTickCount - t) + ' msec');
  Log('Str count='+formatFloat(',0',FStrCount));
  Log('Index size='+formatFloat(',0',FIndexFileSize));

  t := TThread.GetTickCount;
  Log('Sorting');
  Sort();
  Log('Sorting done in '+FormatFloat(',0', TThread.GetTickCount - t) + ' msec');

  t := TThread.GetTickCount;
  Log('Writing results');
  WriteResultFile();
  Log('Writing results done in '+FormatFloat(',0', TThread.GetTickCount - t) + ' msec');

end;




procedure TfSorterMain.bOpenFileClick(Sender: TObject);
begin
  if dlgOpen1.Execute then
    openfile(dlgOpen1.FileName);
end;

// try to open selected file and show its name and size
procedure TfSorterMain.OpenFile(AFilename:string);
var
  f:TFilestream;
begin
  FFilename := AFilename;
  lblFilename.Caption := FFilename;
  f := tfile.OpenRead(FFilename);
  lblFilesize.Caption := FormatFloat(',0', f.Size);
  f.Free;
  bStartStop.Enabled := true;
end;

procedure TfSorterMain.bStartStopClick(Sender: TObject);
begin
  if State=sStopped then
    State := sRunning
  else
    State := sStopped;
end;

constructor TfSorterMain.Create(AOwner: TComponent);
begin
  inherited;

  lblFilename.Caption := '';
  lblFilesize.Caption := '';
  lblInfo.Caption := '';

  // we can specify an input file in param string
  if FileExists(ParamStr(1)) then
    OpenFile(ParamStr(1));
end;

destructor TfSorterMain.Destroy;
begin
  inherited;
end;

procedure TfSorterMain.Log(s: string);
begin
  m1.Lines.Add(FormatDateTime('hh:nn:ss ', now) + s);
end;

procedure TfSorterMain.SetState(const Value: TState);
begin
  FState := Value;
  if Value=sStopped then
  begin
    bStartStop.Caption := '������';
    if FSortThread<>nil then
      FSortThread.Terminate;
  end
  else
  begin
    bStartStop.Caption := '����������';
    StartSorting();
  end;
end;

procedure TfSorterMain.StartSorting();
begin
  if rbAlf.Checked then
    FSortThread := TSortThread.Create(Handle, ExtractFilePath(ParamStr(0))+'parts\', FFilename, SortTerm)
  else
    FSortThread := TSort2Thread.Create(Handle, ExtractFilePath(ParamStr(0))+'parts\', FFilename, SortTerm);
  FSortThread.Start;
end;

procedure TfSorterMain.UmLblText(var Message: TMessage);
var
  s:TString;
begin
  s := TString(Message.LParam);
  lblInfo.Caption := s.s;
  s.Free;
end;

procedure TfSorterMain.UmLog(var Message: TMessage);
var
  s:TString;
begin
  s := TString(Message.LParam);
  Log(s.s);
  s.Free;
end;

procedure TfSorterMain.UmSetMax(var Message: TMessage);
begin
  pb1.Max := message.LParam;
end;

procedure TfSorterMain.UmSetPos(var Message: TMessage);
begin
  pb1.Position := message.LParam;
end;

// sort thread's OnTerminate handler
procedure TfSorterMain.SortTerm(Sender:TObject);
begin
  FSortThread := nil;
  State := sStopped;
end;


end.
