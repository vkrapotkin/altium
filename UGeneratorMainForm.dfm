object fGeneratorMain: TfGeneratorMain
  Left = 0
  Top = 0
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1075#1077#1085#1077#1088#1072#1090#1086#1088#1072
  ClientHeight = 494
  ClientWidth = 700
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object lblDict: TLabel
    Left = 24
    Top = 8
    Width = 28
    Height = 13
    Caption = 'lblDict'
  end
  object eLineCount: TLabeledEdit
    Left = 337
    Top = 395
    Width = 153
    Height = 21
    EditLabel.Width = 133
    EditLabel.Height = 13
    EditLabel.Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1089#1090#1088#1086#1082' (x1000)'
    TabOrder = 0
    Text = '10000'
    OnKeyPress = eLineCountKeyPress
  end
  object eWordCountMin: TLabeledEdit
    Left = 24
    Top = 395
    Width = 121
    Height = 21
    EditLabel.Width = 87
    EditLabel.Height = 13
    EditLabel.Caption = #1057#1083#1086#1074' '#1074' '#1089#1090#1088#1086#1082#1077' '#1086#1090
    TabOrder = 1
    Text = '2'
  end
  object eWordCountMax: TLabeledEdit
    Left = 168
    Top = 395
    Width = 121
    Height = 21
    EditLabel.Width = 88
    EditLabel.Height = 13
    EditLabel.Caption = #1057#1083#1086#1074' '#1074' '#1089#1090#1088#1086#1082#1077' '#1076#1086
    TabOrder = 2
    Text = '10'
  end
  object pb1: TProgressBar
    Left = 24
    Top = 440
    Width = 649
    Height = 17
    TabOrder = 3
  end
  object bStartStop: TButton
    Left = 504
    Top = 390
    Width = 169
    Height = 31
    Caption = #1053#1072#1095#1072#1090#1100
    TabOrder = 4
    OnClick = bStartStopClick
  end
  object m1: TMemo
    Left = 24
    Top = 27
    Width = 649
    Height = 337
    ScrollBars = ssBoth
    TabOrder = 5
    WantReturns = False
    WordWrap = False
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '.txt'
    FileName = 'file.txt'
    Filter = #1058#1077#1082#1089#1090#1086#1074#1099#1077' '#1092#1072#1081#1083#1099' (*.txt)|*.txt'
    Title = #1057#1086#1093#1088#1072#1085#1077#1085#1080#1077' '#1092#1072#1081#1083#1072
    Left = 600
    Top = 128
  end
end
