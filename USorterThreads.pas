unit USorterThreads;

interface

uses
  System.Classes, System.generics.collections, Vcl.ComCtrls, Vcl.StdCtrls,
  Winapi.Messages, System.SysUtils;

type
  // class to transfer strings through the windows messages
  // sending side allocates memory
  // receiving side frees memory
  TString = class
    s:string;
    class function Make(AString:string):TString;
  end;

  TSortThread = class(TThread)
  const
    LIST_CAPACITY = 1000000; // chunk size measured in strings
    BUF_SIZE = 4096;
    BUF_SIZE_W = 4096;
  private
    function PrepareLineForSorting(sb: TStringBuilder;
      const s: string): boolean;
  protected
    FLastUpdate:int64;
    FHandle:THandle;
    FSourceFileName: string;
    FLinesTotal: NativeInt;

    // a little bit faster writing to output stream than StreamWriter
    procedure WriteLine(w: TBufferedFileStream; s: string);
    // rebuild an original string
    procedure RestoreAndWrite(s:string; sb:TStringBuilder; w:TBufferedFileStream);
    // clean the directory where part files stored
    procedure CleanPartsDirectory;//10*1024*1024;

    procedure Log(AString:string);
    function TimeToUpdate(interval: int64): boolean; inline;

    // step 1 - split source file
    procedure SplitSourceFile;
    // step 2 - process every file - do sort inside
    procedure ProcessFile(AIndex: integer);
    // step 3 - merge them back in
    procedure MergeOutFile;

  public
    // path where part files stored
    FDestPath: string;
    // list of filenames
    FFiles: TStringList;

    procedure Execute; override;

    constructor Create(AHandle:THandle; ADestPath, ASourceFile: string; ATermProc: TNotifyEvent);
    destructor Destroy; override;
  end;

  TSort2Thread = class(TSortThread)
  public
    // step 1 - split source file
    procedure SplitSourceFile;
    procedure MergeOutFile();
    procedure Execute; override;
  end;

const
  UM_SET_MAX = WM_USER + 100;
  UM_SET_POS = WM_USER + 101;
  UM_LBL_TEXT = WM_USER + 102;
  UM_LOG = WM_USER + 103;

implementation

uses
  System.IOUtils, Winapi.Windows, system.threading;

{ TSortThread }

constructor TSortThread.Create(AHandle:THandle; ADestPath, ASourceFile: string; ATermProc: TNotifyEvent);
begin
  inherited Create(True);
  FreeOnTerminate := True;
  OnTerminate := ATermProc;
  FDestPath := ADestPath;
  FSourceFileName := ASourceFile;
  FFiles := TStringList.Create;
  FHandle := AHandle;
end;


destructor TSortThread.Destroy;
begin
  FreeAndNil(FFiles);
  inherited;
end;


procedure TSortThread.WriteLine(w:TBufferedFileStream; s:string);
var
  arr:TBytes;
begin
  arr := TEncoding.UTF8.GetBytes(s+#13#10);
  w.Write(arr, Length(arr));
end;


// Details of implementation
// 1.Split source file to smaller files according to first letter of string
// 1.1 to avoid a separate sorting for strings and numbers
// move numbers in the end of string, left padding zeros to length of 10
// 2. sort files in memory and write back reverting numbers in the beginning of string
// 2.1 if file is not small enough then split it into really small chunks
// then sort them then merge them back
// 3. merge back the whole big file from the sorted files by alphabet
procedure TSortThread.Execute;
var
  Pool: TThreadPool;
  StartTime:TDateTime;
begin
  try
    StartTime:=now;
    Log('��������� �� �����');
    SplitSourceFile();
    if Terminated then
      exit;

    Log('��������� �� ����� ���������');

    Log('���������� �������� ���������� ������');
    postmessage(FHandle, UM_SET_MAX, 0, FFiles.Count);

    // multi-thread processing of files
    Pool := TThreadPool.Create;
    Pool.SetMinWorkerThreads(TThread.ProcessorCount*10);
    Pool.SetMaxWorkerThreads(TThread.ProcessorCount*25);
    TParallel.For(0, FFiles.Count-1,
      procedure (i:Integer)
      begin
        ProcessFile(i);
      end,
      pool);

    PostMessage(FHandle, UM_SET_POS, 0, FFiles.Count);
    Log('���������� ���������');
    // sort filenames for correct order in result file
    FFiles.UseLocale;
    FFiles.CaseSensitive := True;
    FFiles.Sort;
    MergeOutFile();
    Log(#13#10'������ ���������'#13#10'��������� ������� '+FormatDateTime('hh:nn:ss',now-StartTime));
  except on e:Exception do
    begin
      Log('������ '+e.Message);
    end;
  end;
end;


procedure TSortThread.Log(AString: string);
begin
  PostMessage(FHandle, UM_LOG, 0 , nativeint(TString.Make(AString)));
end;

procedure TSortThread.CleanPartsDirectory();
var
  FileName: string;
begin
  if TDirectory.Exists(FDestPath) then
  begin
    for FileName in TDirectory.GetFiles(FDestPath, '*.*') do
      TFile.Delete(FileName);
  end
  else
    ForceDirectories(FDestPath);
end;

function TSortThread.PrepareLineForSorting(sb: TStringBuilder; const s:string):boolean;
var
  p: integer;
  number: string;
begin
  p := s.IndexOf('.');
  if p = -1 then
    Exit(false);

  number := s.Substring(0, p);
  sb.Length :=0;
  sb.Append(s.Substring(p + 2)).Append('.')
    .Append('0000000000'.Substring(0, 10 - p)).Append(number);
  result := true;
end;



procedure TSortThread.SplitSourceFile();
var
  r: TBufferedFileStream;
  w: TBufferedFileStream;
  s: string;
  sb: TStringBuilder;
  key: string;
  LineCounter: integer;
  FileName: string;
  WriteStreams: TObjectDictionary<string, TBufferedFileStream>;
  rd: TStreamReader;
  startTime:Int64;

  procedure UpdateUI();
  var
    s:TString;
    d:Int64;
  begin
    postmessage(FHandle, UM_SET_POS, 0, Round(r.Position / r.Size *100));
    d:=GetTickCount-startTime;
    if d<>0 then
    begin
      s:=TString.Make('������: ' + WriteStreams.Count.ToString +
                    ' �����: ' + FormatFloat(',0',LineCounter) +
                    ' (' + FormatFloat(',0',LineCounter/d*1000)+ '/���)' );
      postmessage(FHandle, UM_LBL_TEXT, 0, nativeint(s));
    end;
  end;
begin
  CleanPartsDirectory();
  // Make a dictionary of writers, indexed by alfabet leter
  WriteStreams := TObjectDictionary<string, TBufferedFileStream>.Create([doOwnsValues]);
  // StringBuilder to avoid memory manager rush
  sb := TStringBuilder.Create(2048);
  // Buffered stream dramaticatty speeds up extracting of strings
  r := TBufferedFileStream.Create(FSourceFileName, fmOpenRead, BUF_SIZE);
  rd := TStreamReader.Create(r, TEncoding.UTF8, False);
  rd.OwnStream;
  try
    PostMessage(FHandle, UM_SET_MAX, 0, 100);

    startTime := GetTickCount;
    LineCounter := 0;

    while (not rd.EndOfStream) and (not Terminated) do
    begin
      s:= rd.ReadLine;
      // move number from start to end of line
      // error never can be reached, but let handle it
      if not PrepareLineForSorting(sb, s) then
        Continue;
      // get appropriate writer and write down the string
//      key := sb.Chars[0] + '_' + IntToHex(Ord(sb.Chars[0]), 4);
      key := IntToHex(Ord(sb.Chars[0]), 4);
      if not WriteStreams.TryGetValue(key, w) then
      begin
        FileName := FDestPath + 'part_' + key + '.txt';
        FFiles.Add(filename);
        w := TBufferedFileStream.Create(filename, fmCreate, BUF_SIZE_W);
        WriteStreams.Add(key, w);
      end;
      WriteLine(w,sb.ToString);

      if TimeToUpdate(500) then
        UpdateUI();
      inc(LineCounter);
    end;
    UpdateUI();
    FLinesTotal := LineCounter;
    // flush all writers
    for key in WriteStreams.Keys do
    begin
      w := WriteStreams[key];
      w.FlushBuffer;
    end;
  finally
    WriteStreams.Free;
    rd.Free;
    sb.Free;
  end;
end;


function TSortThread.TimeToUpdate(interval:int64):boolean;
begin
  result := tthread.gettickcount-FLastUpdate >= interval;
  if result then
    FLastUpdate := tthread.gettickcount;
end;


// restores original string
// strip number and put in the beginning of line
procedure TSortThread.RestoreAndWrite(s:string; sb:TStringBuilder; w:TBufferedFileStream);
var
  p1,p2:integer;
begin
  p2 := s.LastIndexOf('.');
  p1 := p2+1;
  while s.Chars[p1]='0' do
    inc(p1);
  sb.Length :=0;
  sb.Append(s.Substring(p1)).Append('. ').Append(s.Substring(0,p2));
  WriteLine(w, sb.ToString);
end;


// implements sorting of prepared alphabet file
// external merge way
procedure TSortThread.ProcessFile(AIndex: integer);
var
  Chunks:TStringList;
  filename:string;

  procedure UpdateUI(msg:string);
  var
    s:TString;
  begin
    s:=TString.Make('����: ' + filename+ ' ' + msg);
    postmessage(FHandle, UM_LBL_TEXT, 0, nativeint(s));
  end;

  // split same-start-letter file to chunks that can be sorted in memory
  // i.e. part_A_0041.txt to
  // part_A_0041.0000
  // part_A_0041.0001
  // part_A_0041.0002
  procedure SplitToChunks(filename:string);
  var
    ChunkCount:integer;
    r:TBufferedFileStream;
    rd:TStreamReader;
    w:TBufferedFileStream;
    // this list do the actual sorting
    // StringList can't be used because it can't handle Capacity
    lst : TList<string>;
    s:string;
    fname, chunkName : string;
    i: Integer;
  begin
    fname := filename.Substring(0, filename.LastIndexOf('.'));
    r:=TBufferedFileStream.Create(filename, fmOpenRead);
    rd:=TStreamReader.Create(r, TEncoding.UTF8, false, BUF_SIZE);
    rd.OwnStream;
    lst := TList<string>.Create();
    try
      lst.Capacity := LIST_CAPACITY;
      ChunkCount:=0;
      w:=nil;
      while (not rd.EndOfStream) do
      begin
        s:=rd.ReadLine;
        if (w=nil)
        or (lst.count>=LIST_CAPACITY)then
        begin
          if w<>NIL then
          begin
            lst.Sort;
            for i := 0 to lst.count-1 do
            begin
              WriteLine(w,lst[i]);
            end;
            w.FlushBuffer;
            w.Free;
          end;
          chunkName := fname + '.' + formatfloat('0000',ChunkCount);
          Chunks.Add(chunkName);
          if FileExists(chunkName) then
            TFile.Delete(chunkName);

          w := TBufferedFileStream.Create(chunkName, fmCreate, BUF_SIZE_W);
          lst.Clear;
          inc(ChunkCount);
          UpdateUI(' ��������� '+ChunkCount.tostring);
        end;
        lst.Add(s);
      end;

      // save last chunk
      if w<>NIL then
      begin
        lst.Sort;
        for i := 0 to lst.count-1 do
          WriteLine(w,lst[i]);
        w.FlushBuffer;
        w.Free;
      end;
    finally
      lst.free;
      rd.Free;
    end;
  end;

  // implements external merge algorhythm
  // contains list of sources
  // read one string from every source into stringList
  // sort stringlist
  // most first line write down to output file
  // when source is exhausted, delete it from list
  procedure MergeSortedChunks(filename:string);
  var
    w : TBufferedFileStream;
    readers : TObjectList<TStreamReader>;
    readerStream : TBufferedFileStream;
    compList: TStringList;
    i: integer;
    tmpReader: TStreamReader;
    sb: TStringBuilder;


  begin
    UpdateUI('�������');
    if FileExists(filename) then
      TFile.Delete(filename);
    w := TBufferedFileStream.Create(filename, fmCreate, BUF_SIZE_W);
    readers := TObjectList<TStreamReader>.Create();
    CompList := TStringList.Create;
    sb:=TStringBuilder.Create(4096);
    try
      CompList.CaseSensitive := True;
      CompList.UseLocale := True;
      compList.Sorted := true;
      for i := 0 to Chunks.Count-1 do
      begin
        readerStream := TBufferedFileStream.Create(Chunks[i], fmOpenRead, BUF_SIZE) ;
        readers.Add( TStreamReader.Create(readerStream, false) );
        readers.Last.OwnStream;
      end;

      FLastUpdate:=0;
      // optimization.
      // if we have only one source then we don't need to sort sources in copmList
      if readers.count=1 then
      begin
        i := 0;
        while (not readers[0].EndOfStream) and (not Terminated) do
        begin
          RestoreAndWrite(readers[0].ReadLine(), sb, w);
          if  TimeToUpdate(500) then
            updateui('������� '+ FormatFloat(',0', i));
          Inc(i);
        end;
        readers.Delete(0);
      end
      else
      if readers.count>0 then
      begin
        for i := 0 to readers.Count-1 do
          compList.AddObject(readers[i].ReadLine(), readers[i]);

        i:=0;
        repeat
          RestoreAndWrite(compList[0], sb, w);

          tmpReader := compList.Objects[0] as TStreamReader;
          if tmpReader.EndOfStream then
          begin
            readers.Remove(tmpReader);
            compList.Delete(0);
          end
          else
          begin
            compList.Delete(0);
            compList.AddObject( tmpReader.ReadLine(), tmpReader);
          end;
          if TimeToUpdate(500) then
            updateui('������� '+ FormatFloat(',0', i));
          Inc(i);
        until (readers.Count=0) or Terminated;
      end;
      updateui('������� '+ FormatFloat(',0', i));

      for i := 0 to Chunks.Count-1 do
        TFile.Delete(Chunks[i]);

    finally
      w.FlushBuffer;
      w.Free;
      sb.Free;
      compList.Free;
      readers.Free;
    end;
  end;

begin
  filename := FFiles[AIndex];
  PostMessage(FHandle, UM_SET_POS, 0, AIndex);
  Chunks:=TStringList.Create;
  try
    SplitToChunks(filename);
    TFile.Delete(filename);
    MergeSortedChunks(filename);
  finally
    Chunks.Free;
  end;
end;


// merge back an output file from the "alphabet" part files
procedure TSortThread.MergeOutFile();
var
  i: Integer;
  r, w: TBufferedFileStream;
  size: int64;
  outFileName : string;
  BOM: integer;
begin
  Log('�������� ������� ������');
  outFileName := ChangeFileExt(FSourceFileName, '.out.txt');
  if FileExists(outFileName) then
    tfile.Delete(outFileName);

  w:=TBufferedFileStream.Create(outFileName, fmCreate, BUF_SIZE);
  r:=nil;
  try
    for i := 0 to FFiles.Count-1 do
    begin
      if r<>NIL then
        r.Free;
      r:=TBufferedFileStream.Create(FFiles[i], fmOpenRead);
      // remove UTF BOM marker
      BOM := 0;
      r.Read(bom, 3);
      size := r.size - 3;
      if BOM <> $00BFBBEF then
      begin
        r.Position :=0;
        inc(size,3);
      end;
      w.CopyFrom(r,size);
     end;
    Log('������� ������ ���������');
    Log('�������� ����: '+outFileName);
    Log('������: '+FormatFloat(',0', w.Size));
  finally
    FreeAndNil(w);
    FreeAndNil(r);
  end;
end;


{ TString }

class function TString.Make(AString: string): TString;
begin
  Result := TString.Create();
  Result.s := AString;
end;

{ TSort2Thread }

procedure TSort2Thread.Execute;
var
  StartTime:TDateTime;
begin
  try
    StartTime:=now;
    Log('��������� �� �����');
    SplitSourceFile;
    Log('��������� �� ����� ���������');

    Log('���������� �������� ���������� ������');
    MergeOutFile;
    Log('���������� ���������');
    Log(#13#10'������ ���������'#13#10'��������� ������� '+FormatDateTime('hh:nn:ss',now-StartTime));
  except on e:Exception do
    begin
      Log('������ '+e.Message);
    end;
  end;
end;

procedure TSort2Thread.MergeOutFile;
var
  Readers : TObjectList<TStreamReader>;
  r: TStreamReader;
  st: TBufferedFileStream;
  w: TBufferedFileStream;
  i: Integer;
  outFileName : string;
  templist: TStringList;
  s: string;
  sb : TStringBuilder;
  LineCounter: NativeInt;
  startTime : cardinal;

  procedure UpdateUI();
  var
    s:TString;
    d:Int64;
  begin
    postmessage(FHandle, UM_SET_POS, 0, Round(LineCounter / flinestotal *100));
    d:=GetTickCount-startTime;
    if d<>0 then
    begin
      s:=TString.Make('������: ' + Readers.Count.ToString +
                    ' �����: ' + FormatFloat(',0',LineCounter) +
                    ' (' + FormatFloat(',0',LineCounter/d*1000)+ '/���)' );
      postmessage(FHandle, UM_LBL_TEXT, 0, nativeint(s));
    end;
  end;

begin
  LineCounter := 0;
  startTime := TThread.GetTickCount;
  postmessage(FHandle, UM_SET_MAX, 0, 100);

  outFileName := ChangeFileExt(FSourceFileName, '.out.txt');
  Readers := TObjectList<TStreamReader>.Create();
  if FileExists(outFileName) then
    TFile.Delete(outfilename);
  w := TBufferedFileStream.Create(outFileName, fmCreate, 2048);
  TempList:=TStringList.Create;
  sb := TStringBuilder.Create(4096);
  try
    // create the list of readers
    for i := 0 to FFiles.Count-1 do
    begin
      st := TBufferedFileStream.Create(FFiles[i], fmOpenRead, 2048);
      r := TStreamReader.Create(st, TEncoding.UTF8, true);
      r.OwnStream;
      Readers.Add(r);
    end;

    // read first string from every reader
    for i := 0 to Readers.Count-1 do
    begin
      r := Readers[i];
      s := r.ReadLine;
      templist.AddObject(s,r);
    end;
    templist.Sort;
    templist.Sorted := true;
    templist.Duplicates := TDuplicates.dupAccept;

    // merge sorting implementation
    // select smallest string from every already sorted source
    repeat
      RestoreAndWrite(templist[0], sb, w);
      r := TStreamReader(templist.Objects[0]);
      templist.Delete(0);
      if not r.EndOfStream then
        templist.AddObject(r.ReadLine, r);
      if TimeToUpdate(500) then
        UpdateUI();
      Inc(linecounter);
    until (templist.Count=0) or (terminated);
    UpdateUI();
    Log('�������� �����:'+FormatFloat(',0', linecounter));

  finally
    FreeAndNil(templist);
    FreeAndNil(sb);
    FreeAndNil(w);
    FreeAndNil(Readers);
  end;
end;

procedure TSort2Thread.SplitSourceFile;
var
  WriteStreams: TObjectList<TBufferedFileStream>;
  sb: TStringBuilder;
  r: TBufferedFileStream;
  w: TBufferedFileStream;
  rd: TStreamReader;
  startTime: Int64;
  LineCounter: integer;
  LinesInFile: integer;
  s: string;
  TempList:TStringList;
  FileName: string;

  procedure UpdateUI();
  var
    s:TString;
    d:Int64;
  begin
    postmessage(FHandle, UM_SET_POS, 0, Round(r.Position / r.Size *100));
    d:=GetTickCount-startTime;
    if d<>0 then
    begin
      s:=TString.Make('������: ' + WriteStreams.Count.ToString +
                    ' �����: ' + FormatFloat(',0',LineCounter) +
                    ' (' + FormatFloat(',0',LineCounter/d*1000)+ '/���)' );
      postmessage(FHandle, UM_LBL_TEXT, 0, nativeint(s));
    end;
  end;

  procedure SaveFile;
  begin
    TempList.Sort;
    TempList.SaveToStream(w, TEncoding.UTF8);
    LinesInFile := 0;
  end;

begin
  CleanPartsDirectory();
  WriteStreams := TObjectList<TBufferedFileStream>.Create();
  sb := TStringBuilder.Create(2048);
  r := TBufferedFileStream.Create(FSourceFileName, fmOpenRead, BUF_SIZE);
  rd := TStreamReader.Create(r, TEncoding.UTF8, False);
  rd.OwnStream;
  TempList := TStringList.Create;
  try
    PostMessage(FHandle, UM_SET_MAX, 0, 100);

    startTime := GetTickCount;
    LineCounter := 0;
    LinesInFile := 0;

    while (not rd.EndOfStream) and (not Terminated) do
    begin
      s:= rd.ReadLine;
      // move number from start to end of line
      // error never can be reached, but let handle it
      if not PrepareLineForSorting(sb, s) then
        Continue;

      if LinesInFile=0 then
      begin
        FileName := FDestPath + 'part_' + FormatFloat('00000',WriteStreams.count) + '.txt';
        FFiles.Add(filename);
        w := TBufferedFileStream.Create(filename, fmCreate, BUF_SIZE_W);
        WriteStreams.Add(w);
        TempList.Clear;
      end;
      TempList.Add(sb.ToString);

      if TimeToUpdate(500) then
        UpdateUI();
      inc(LineCounter);
      Inc(LinesInFile);

      if LinesInFile>100000 then //
        SaveFile();
    end;
    if LinesInFile<>0 then
      SaveFile();
    FLinesTotal := LineCounter;
    UpdateUI();

  finally
    FreeAndNil(r);
    FreeAndNil(TempList);
    FreeAndNil(WriteStreams);
    FreeAndNil(sb);
  end;
end;

end.
