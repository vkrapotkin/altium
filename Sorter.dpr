program Sorter;

uses
  Vcl.Forms,
  USorterMainForm in 'USorterMainForm.pas' {fSorterMain},
  USorterThreads in 'USorterThreads.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfSorterMain, fSorterMain);
  Application.Run;
end.
